<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">

    <title>Document</title>
</head>

<body>


    <h2 > Seguro Saúde <h2>

            <div id="form" class="container row justify-content-center">

            
                <form action="Seguro-saude-calculo.php" method="post" class="col-4">
                    <h3 > Proposta de Segurado <h3>

                    <div class="mb-1">
                        <label for="inp_name" class="form-label">Nome</label><br>
                        <input type="text" name="inp_name" class="form-control"><br>
                    </div>

                    <div class="mb-1">
                        <label for="inp_sobrenome" class="form-label">Sobrenome</label><br>
                        <input type="text" name="inp_sobrenome" class="form-control">
                    </div>

                    <div class="justify-content-center">


                        <div class="form-check form-check">
                            <input class="form-check-input-sm" type="radio" id="inlineCheckbox1" value=1 name="r1">
                            <label class="form-check-label-sm" for="inlineCheckbox1">Menor que 20</label>
                        </div>
                        <div class="form-check form-check">
                            <input class="form-check-input-sm" type="radio" id="inlineCheckbox1" value=2 name="r1">
                            <label class="form-check-label-sm" for="inlineCheckbox1">21 - 30</label>
                        </div>
                        <div class="form-check form-check">
                            <input class="form-check-input-sm" type="radio" id="inlineCheckbox1" value=3 name="r1">
                            <label class="form-check-label-sm" for="inlineCheckbox1">31 - 40</label>
                        </div>
                        <div class="form-check form-check">
                            <input class="form-check-input-sm" type="radio" id="inlineCheckbox1" value=4 name="r1">
                            <label class="form-check-label-sm" for="inlineCheckbox1">41 - 50</label>
                        </div>
                        <div class="form-check form-check">
                            <input class="form-check-input-sm" type="radio" id="inlineCheckbox1" value=5 name="r1">
                            <label class="form-check-label-sm" for="inlineCheckbox1">51 - 65</label>
                        </div>
                        <div class="form-check form-check">
                            <input class="form-check-input-sm" type="radio" id="inlineCheckbox1" value=6 name="r1">
                            <label class="form-check-label-sm" for="inlineCheckbox1">66 ou mais</label>

                        </div>
                      
                        <div class="form-check form-check">
                            <input class="form-check-input-sm" type="checkbox" id="flexCheckDefault" value="True" name="doenca">
                            <label class="form-check-label-sm" for="flexCheckDefault">Possui doença prévia</label>

                        </div>
                        

                    </div>
                    <br>



                    <button type="submit" value="enviar" class="btn btn-primary">Enviar</button>
                </form>

            </div>

</body>

</html>