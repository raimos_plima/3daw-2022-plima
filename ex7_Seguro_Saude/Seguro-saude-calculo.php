<?php

$nome=NULL;
$sobrenome=NULL;
$doenca=filter_input(INPUT_POST,"doenca")==="True"? "SIM"  : "NAO";
$faixa=filter_input(INPUT_POST,"r1",FILTER_SANITIZE_NUMBER_INT);

if(isset($_POST["inp_name"])){
    $nome=filter_input(INPUT_POST,"inp_name",FILTER_SANITIZE_STRING);}

if(isset($_POST["inp_sobrenome"])){
    $sobrenome=filter_input(INPUT_POST,"inp_sobrenome",FILTER_SANITIZE_STRING);
}

function calculaOrcamento( $f, $d){

    $valor=200;

    if ($f==2){
        $valor=$valor*1.5;
    }
    else if ($f==3){

        $valor=$valor*2.25;
    }
    else if ($f==4){
        $valor=$valor*5.0625;
    }
    else if ($f==5){

        $valor=$valor*7.59375;
    }
    
    else if($f>5){
        $valor=$valor*11.390625;
    }

    if($d==="SIM"){

        $valor=$valor*1.3;
        
    }

    return $valor;
}

$valor=calculaOrcamento($faixa,$doenca);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>

   
</head>
<body>
    

    <div class="container row justify-content-center">
        <div id="resposta" class="col-4"> 

        <h3 >O envio do formulario é:</h3>
        <?php
        

        echo ("<p> Nome:  <strong>$nome </strong></p>");
        echo ("<p> Sobrenome: <strong> $sobrenome </strong> </p> ");
        echo ("<p> Faixa Etaria:  <strong> $faixa  </strong></p>  ");
        echo ("<p> Possui doença previa:<strong> $doenca </strong> </p>  ");
        echo ("<p>A cotacao do seguro ficou em <strong> $valor</strong> </p>");
        ?>
        </div>
    </div>

   
    
</body>
</html>  