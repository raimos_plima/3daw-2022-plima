
<?php
	session_start();

	$msgErro = null;
	if( isset( $_SESSION["flash"])) {
		
		$msgErro = $_SESSION["flash"];
		unset( $_SESSION["flash"]);
	}
	$logado = isset( $_SESSION["usuario"] );

	// date_default_timezone_set('America/Sao_Paulo');
	// $d = date("H");
	// if( $d < 12 ) $saudacao = "Bom dia";
	// elseif ($d < 17) $saudacao = "Boa tarde";
	// else  $saudacao = "Boa noite";
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="login.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&display=swap" rel="stylesheet">
    
</head>
<body>

    <div class="container"> 

        <div id="login">

            <h2>Autenticação</h2>
            <div  class="separador"> </div>

            <?php
                $saudacao="";
                if ($logado){?>

                    <span><?= $saudacao . ',' . $_SESSION["usuario"] ?></span>
            
                    <?php }else {
               
                    if( $msgErro != null ) {
            ?>
            <p style="color: red;"><?= $msgErro ?></p>
    
            <?php	} }?>


            <form method="POST" action="/abrir_sessao.php" >
                <label for="name">Login</label>
                <input type="text" id="fname" name="name" value="">
                <label for="password">Senha</label>
                <input type="password" id="fname" name="password" value="">
                <button class="botaoprincipal"  type="submit" name="btn_login">Login</button>
            </form>

            <a href="/recuperar/recuperar.php">Esqueceu a senha</a>

            <div  class="separador"> </div>

            <h4>Ainda nao é usuário?</h4>
            <a href="/cadastro/cadastro.php">Cadastre-se</a>

            
        </div>
        <div class="azul"></div>

    </div>


    
</body>
</html>