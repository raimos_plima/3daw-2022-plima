<DOCTYPE html>
    <html lang="pt-br">

        <?php
    
        $produtos = array(
            0 => array('img' =>'celular.jpg', 'titulo'=>'Celular', 'desc' => 'Smartphone Samsung Galaxy S21 128GB 5G Wi-Fi Tela 6.2\'\' Dual Chip 8GB RAM Câmera Tripla + Selfie 10MP - Cinza'), 
            1 => array('img' =>'bike.jpg', 'titulo'=>'Bicicleta', 'desc' => 'Bicicleta Aro 26 Ultra Bikes Masculino 18v Freios V-Brake'), 
            2 => array('img' =>'videoGame.jpg', 'titulo'=>'Video Game', 'desc' => 'Videogame PlayStation 2 Slim Standard Black Console'),
            3 => array('img' =>'fone.jpg', 'titulo'=>'Fone', 'desc' => 'Fone de Ouvido Bluetooth JBL Tune 660NC On Ear Preto - JBLT660NCBLK'),
            4 => array('img' =>'monitor.jpg', 'titulo'=>'Monitor', 'desc' => 'Monitor LG 22\'\' LED Full HD'),
            5 => array('img' =>'tv.jpg', 'titulo'=>'Televisão', 'desc' => 'Smart TV HD LED 32” Samsung 32T4300A - Wi-Fi HDR 2 HDMI 1 USB'),
            6 => array('img' =>'radio.jpg', 'titulo'=>'Radio', 'desc' => 'Caixa Multimidia Portatil Tuner FM Preta'),
            7 => array('img' =>'ventilador.jpg', 'titulo'=>'Ventilador', 'desc' => 'Ventilador de Mesa Arno Ultra Silence Force VD40 40cm, 6 Pás, 126W - Preto')
        ); 
    
        //novo produto
        $titulo = filter_input(INPUT_POST, "titulo", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $descricao = filter_input(INPUT_POST, "descricao", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        
        if(isset($titulo)){
            //imagem
            $dir = "img/";
            $file = $dir.basename($_FILES["foto"]["name"]);
            move_uploaded_file($_FILES["foto"]["tmp_name"], $file);
            //array novo produto
            $arrayAux = array("img" => $_FILES["foto"]["name"], "titulo" => "$titulo", "desc" => "$descricao");        
            array_unshift($produtos, $arrayAux);
        }
    
    
        $qtd = 3;
        $atual = (isset($_GET['pag']))? intval($_GET['pag']):1;   
        $pagProd = array_chunk($produtos, $qtd);
        $contar = count($pagProd);
        if($contar>0){ 
            $result = $pagProd[$atual -1];
        }else{
            $result = 0;
        }
        
        function paginacao($contar, $atual){
            $inicial = 1;
            $ultima = $contar;
            $prev = $atual-1;
            $next = $atual+1;
            
            echo "<a href ='?pag=$inicial'>  <<  </a>";
            if($atual==1){
                echo "<a href ='#'>  Prev  </a>";
            }else{
            echo "<a href ='?pag=$prev'>  Prev  </a>";
            }
            for($i=1; $i <= $contar; $i++){
                if($i == $atual){ 
                    echo "<a href='#'>  $i  </a>";
                }else{
                    echo "<a href='?pag=$i'>  $i  </a>";
                }
            }
            if($atual==$contar){
                echo "<a href ='#'>  Next  </a>";
            }else{
            echo "<a href ='?pag=$next'>  Next  </a>";
            }
            echo "<a href ='?pag=$ultima'>  >>  </a>";
        } 
            
    ?>
    
    
    
    
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>EcoEscambo - Ofertas Recebidas</title>
        <link rel="stylesheet" href="style1.css">


    </head>

    <body>

        <div class="container">

            <div class="foto">

                <img src="produto/p1.jpg">

            </div>

            <div id="meuproduto">

                <h2> Alexa </h2>
                <section><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex voluptates reiciendis harum debitis modi natus obcaecati odit porro eaque veniam facere tenetur, possimus maxime autem quae. Vel tempore reiciendis omnis. </p> </section>

            </div>


            <div class="separador"> </div>

            <div id="interessados">

                <a>Interresado(s) 3</a>

            </div>

            <div id="menu">

                <div id="coluna1">
                    <p>Interressado</p>
                    <br><br><br><br><br>
                    <p>Aceita troca por</p>
                                         
                </div>


                <div id="coluna2">
                    
                    <select>
                        <option>Selecione</option>
                        <option>José Marcos</option>
                        <option>Paulo André</option>
                        <option>Roberta Silva</option>
                    </select>
                    <table>
                    <?php                      
                        if($result == 0){
                            echo "<b>Você não possui produtos cadastrados !</b>";
                        }else{
                            foreach($result as $value){ 
                                echo
                                    "<tr>
                                        <td>
                                            <img src='img/{$value['img']}' class='foto'>
                                        </td>
                                        <td id='descr'>
                                            <b>{$value['titulo']}</b><br>{$value['desc']}
                                        </td>                                
                                        <td>
                                            <button type='submit' id='propor'> Propor </button>
                                        </td>
                                    </tr>"
                                ;
                            }
                        }               
                                            
                    ?>
                </table>
                </div>

              
        </div>


        <div id="coluna2">
                
            </div>
            <br>
            <?php
                    paginacao($contar, $atual);                         
            ?>


        </div>



    </body>

    </html>