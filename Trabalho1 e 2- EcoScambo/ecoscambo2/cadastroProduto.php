<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">    
    <title>Cadastrar produtos</title>
    <link rel="stylesheet" href="style.css"> 
</head>
    <body>
    <div class="container">
        <nav id="menu">

                <ul>

                    <li><a href="catalago.php">Catálogo</a></li>
                    <li><a href="Meus_Produtos.php">Meus produtos</a></li>
                    <li><a href="/login/login.php">Logout</a></li>

                </ul>
        </nav>
        
        <h2>Cadastrar produto</h2>
        
        <div class="separador2"> </div><br><br>
        <form action="Meus_Produtos.php" method="POST" enctype="multipart/form-data">
            <label for="titulo"><b>Titulo breve</b></label><br>
            <input type="text" id="titulo" name="titulo" required><br><br>
            <label for="descricao"><b>Descrição</b></label><br>
            <textarea name="descricao" id="descricao" id="descricao" required></textarea><br>
            <p><b>Foto do produto</b></p>
            <input type="file" name="foto" accept=".png, .jpg, .jpeg" required><br><br>
            <input type="image"src="Botoes/Salvar.png" alt="Submit" id="bEnviar">
        </form>
    </div>
    </body>
</html>