<?php
    require_once(__Dir__ . "/Conexao.php");

    class Produto{
        private $titulo;
        private $descricao;
        private $imagem;
        private $utilizador;

        public function __construct($titulo, $descricao, $imagem, $utilizador){
            $this->titulo = $titulo;
            $this->descricao = $descricao;
            $this->imagem = $imagem;
            $this->utilizador = $utilizador;
        }

        public function add(){
            $pdo = Conexao::abrirConexao();
            $inserir = $pdo->prepare("INSERT INTO produto(titulo, descricao, imagem, id_Utilizador) VALUES (:titulo,:descricao,:imagem,:utilizador)");
            $inserir->bindParam(":titulo", $this->titulo, PDO::PARAM_STR);
            $inserir->bindParam(":descricao", $this->descricao, PDO::PARAM_STR);
            $inserir->bindParam(":imagem", $this->imagem, PDO::PARAM_STR);
            $inserir->bindParam(":utilizador", $this->utilizador, PDO::PARAM_INT);
            return $inserir->execute();
        }

        //Catalago
        public static function qtdReg(){
            $pdo = Conexao::abrirConexao();
            $stmt = $pdo->query("SELECT COUNT(*) FROM produto");
            return $stmt->fetchColumn();
        }

        public static function selecProd($inicial, $qtd){
            $pdo = Conexao::abrirConexao();
            $stmt = $pdo->prepare("SELECT * FROM produto LIMIT $inicial, $qtd");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
        
        //Excluir
        public static function excluir($id){
            
            $pdo = Conexao::abrirConexao();                       
            $excluir = $pdo->prepare("delete from produto where id_produto=:id");
            $excluir->bindParam(":id", $id, PDO::PARAM_INT);
            return $excluir->execute();
        }

        //Meus produtos
        public static function qtdRegMeus(){
            $pdo = Conexao::abrirConexao();
            $stmt = $pdo->query("SELECT COUNT(*) FROM produto WHERE id_Utilizador = 2");
            return $stmt->fetchColumn();
        }

        public static function selecProdMeus($inicial, $qtd){
            $pdo = Conexao::abrirConexao();
            $stmt = $pdo->prepare("SELECT * FROM produto WHERE id_Utilizador = 2 LIMIT $inicial, $qtd");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
    
    }