<?php
    function paginacao($qtdPag, $atual){
        $inicial = 1;
        $ultima = $qtdPag;
        $prev = $atual-1;
        $next = $atual+1;
        
        echo "<a href ='?pag=$inicial'>  <<  </a>";
        if($atual==1){
            echo "<a href ='#'>  Prev  </a>";
        }else{
        echo "<a href ='?pag=$prev'>  Prev  </a>";
        }
        for($i=1; $i <= $qtdPag; $i++){
            if($i == $atual){ 
                echo "<a href='#'>  $i  </a>";
            }else{
                echo "<a href='?pag=$i'>  $i  </a>";
            }
        }
        if($atual==$qtdPag){
            echo "<a href ='#'>  Next  </a>";
        }else{
        echo "<a href ='?pag=$next'>  Next  </a>";
        }
        echo "<a href ='?pag=$ultima'>  >>  </a>";
    }