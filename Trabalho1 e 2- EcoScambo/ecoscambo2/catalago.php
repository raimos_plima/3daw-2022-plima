<?php
    require_once(__DIR__ . "/Classes/Produto.php");

    //paginação banco de dados
    $qtd = 3;
    $atual = (isset($_GET['pag']))? intval($_GET['pag']):1;   
    $qtdProdutos = Produto::qtdReg();
    $qtdPag = ceil($qtdProdutos/$qtd);
    $linhaInicial = ($atual-1)*$qtd;
    $dados = Produto::selecProd($linhaInicial, $qtd);

    require_once("paginacao.php");

?>
<DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Catalogo</title>
        <link rel="stylesheet" href="catalogo.css">


    </head>

    <body>
    
    </nav>
        <div class="container">

            <nav id="menu">

                <ul>

                    <li><a href="">Catálogo</a></li>
                    <li><a href="Meus_Produtos.php">Meus produtos</a></li>
                    <li><a href="/login/login.php">Logout</a></li>

                </ul>

            </nav>


            <div id="busca">

                <h1>Produtos</h1>

                <!-- <div  class="separador"> </div> -->

                <form>
                    <Label> Pesquisa: </Label>
                    <input id="pesquisa" type="text" name="pesquisa" value="">
                    <input class="botaoprincipal" type="submit">

                    <input class="bradio" type="radio" name="pesquisa" value="Todos">
                    <label for="Todos">Todos</label>

                    <input class="bradio" type="radio" name="pesquisa" value="Interesse">
                    <label for="Interesse">Tenho Interesse</label>
                </form>

            </div>

            <div class="separador"> </div>

            <div id="catalogo_produtos">
                <!--<div class="card clearfix">-->
                    
                        <?php foreach($dados as $value){ ?>
                            <table>
                            
                                <tr>
                                    <td>
                                        <!-- <div class="produto">-->
                                            <img src="<?= $value->imagem ?>" id="imgProd">
                                        <!-- </div>-->
                                    </td>
                                    <td>
                                    <!--  <div class="caixa_texto">-->
                                            <h2> <?= $value->titulo ?> </h2>                        
                                            <section><p><?= $value->descricao ?></p> </section>
                                    <!--  </div>-->
                                    </td>
                                    <td>
                                        <div class="div_interesse">
                                            <button type="submit" class="botao_interesse" >Interesse</button>
                                            <!-- <label for="bInteresse" >Interesse </Label> -->
                                        </div>
                                    </td>
                                </tr>
                            
                            </table>
                            <div  class="separador_produto"> </div>
                        <?php } ?>
                   
                <!--</div>-->          
            
                <?php paginacao($qtdPag, $atual); ?>

            </div>
        </div>
    </body>

</html>