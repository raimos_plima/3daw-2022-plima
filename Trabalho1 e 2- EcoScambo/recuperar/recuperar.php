<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recuperacao</title>
    <link rel="stylesheet" href="recuperacao.css">
    <script>
        function confirmaEmail() {
            alert("Sua conta foi desabilitada\nEnviamos a instrução para recuperar seu acesso pelo e-mail!\n");
        }
    </script>
</head>

<body>
    <div class="container">

        <div id="recuperacao">

            <h2>Recuperacão de Senha</h2>
            <div class="separador"> </div>


            <form method="$_POST" onsubmit="confirmaEmail()" action="/cadastro/cadastro.php">
                <label for="EmailRec">E-mail</label>
                <input type="text" id="EmailRec" name="EmailRec" value="">

                <button class="botaoprincipal" name="botaoSubmit" type="submit">Recuperar Acesso</button>

            </form>

            


            <div class="separador"> </div>


            <a href="/login/login.php">Voltar ao Login</a>


        </div>
        <div class="azul"></div>

      

    </div>
</body>

</html>