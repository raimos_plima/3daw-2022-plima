<?php
    
    $produtos = array(
        0 => array('img' =>'celular.jpg', 'titulo'=>'Celular', 'desc' => 'Smartphone Samsung Galaxy S21 128GB 5G Wi-Fi Tela 6.2\'\' Dual Chip 8GB RAM Câmera Tripla + Selfie 10MP - Cinza'), 
        1 => array('img' =>'bike.jpg', 'titulo'=>'Bicicleta', 'desc' => 'Bicicleta Aro 26 Ultra Bikes Masculino 18v Freios V-Brake'), 
        2 => array('img' =>'videoGame.jpg', 'titulo'=>'Video Game', 'desc' => 'Videogame PlayStation 2 Slim Standard Black Console'),
        3 => array('img' =>'fone.jpg', 'titulo'=>'Fone', 'desc' => 'Fone de Ouvido Bluetooth JBL Tune 660NC On Ear Preto - JBLT660NCBLK'),
        4 => array('img' =>'monitor.jpg', 'titulo'=>'Monitor', 'desc' => 'Monitor LG 22\'\' LED Full HD'),
        5 => array('img' =>'tv.jpg', 'titulo'=>'Televisão', 'desc' => 'Smart TV HD LED 32” Samsung 32T4300A - Wi-Fi HDR 2 HDMI 1 USB'),
        6 => array('img' =>'radio.jpg', 'titulo'=>'Radio', 'desc' => 'Caixa Multimidia Portatil Tuner FM Preta'),
        7 => array('img' =>'ventilador.jpg', 'titulo'=>'Ventilador', 'desc' => 'Ventilador de Mesa Arno Ultra Silence Force VD40 40cm, 6 Pás, 126W - Preto')
    ); 

    //novo produto        
    if(isset($_POST['titulo'])){
        $titulo = filter_input(INPUT_POST, "titulo", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $descricao = filter_input(INPUT_POST, "descricao", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        //imagem
        $dir = "img/";
        $file = $dir.basename($_FILES["foto"]["name"]);
        move_uploaded_file($_FILES["foto"]["tmp_name"], $file);
        //array novo produto
        $arrayAux = array("img" => $_FILES["foto"]["name"], "titulo" => "$titulo", "desc" => "$descricao");        
        array_unshift($produtos, $arrayAux);
    }
    
    //paginação
    $qtd = 3;
    $atual = (isset($_GET['pag']))? intval($_GET['pag']):1;   
    $pagProd = array_chunk($produtos, $qtd);
    $contar = count($pagProd);
    if($contar>0){ 
        $result = $pagProd[$atual -1];
    }else{
        $result = 0;
    }
    
    function paginacao($contar, $atual){
        $inicial = 1;
        $ultima = $contar;
        $prev = $atual-1;
        $next = $atual+1;
        
        echo "<a href ='?pag=$inicial'>  <<  </a>";
        if($atual==1){
            echo "<a href ='#'>  Prev  </a>";
        }else{
        echo "<a href ='?pag=$prev'>  Prev  </a>";
        }
        for($i=1; $i <= $contar; $i++){
            if($i == $atual){ 
                echo "<a href='#'>  $i  </a>";
            }else{
                echo "<a href='?pag=$i'>  $i  </a>";
            }
        }
        if($atual==$contar){
            echo "<a href ='#'>  Next  </a>";
        }else{
        echo "<a href ='?pag=$next'>  Next  </a>";
        }
        echo "<a href ='?pag=$ultima'>  >>  </a>";
    } 
        
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">    
        <title>Meus produtos</title>
        <link rel="stylesheet" href="style.css"> 
    </head>
    <body>
                 
        <div class="container">
            <nav id="menu">

                    <ul>

                        <li><a href="catalago.php">Catálogo</a></li>
                        <li><a href="Meus_Produtos.php">Meus produtos</a></li>
                        <li><a href="/login/login.php">Logout</a></li>

                    </ul>

            </nav>
            <div id="busca">
                <h2>Meus Produtos</h2>    
                <form action="Meus_Produtos.php" method="POST">
                    <p>
                        Filtrar:
                        <input type="radio" id="filtro" name="sel_filtro" value="todos" checked>
                        <label for="filtro">Todos</label>
                        <input type="radio" id="inter" name="sel_filtro" value="int">
                        <label for="inter">Só nos quais há interessados</label>
                    </p>
                </form>
            </div>
            
            <div class="separador"> </div>

            <div id="meus_produtos">
                <table>
                    <?php                      
                        if($result == 0){
                            echo "<b>Você não possui produtos cadastrados !</b>";
                        }else{
                            foreach($result as $value){ 
                                echo
                                    "<tr>
                                        <td>
                                            <img src='img/{$value['img']}' id='imgProd'>
                                        </td>
                                        <td id='descr'>
                                            <b>{$value['titulo']}</b><br>{$value['desc']}
                                        </td>                                
                                        <td>
                                            <a href='#'><img src='Botoes/Editar.png' alt='botão Editar' class='botoesint'></a>
                                            <a href='#'><img src='Botoes/Excluir.png' alt='botao Excluir' class='botoesint'></a><br>
                                            <a href='#'><img src='Botoes/interesses.png' alt='botao interesses' id='botoesint'></a>
                                        </td>
                                    </tr>"
                                ;
                            }
                        }               
                                            
                    ?>
                </table>
            </div>
            <br>
            <?php
                paginacao($contar, $atual);                 
            ?>
            
            <hr>
            <a href="cadastroProduto.php">
                <img src="Botoes/Cadastrar.png" alt="Botão cadastrar novo" id="cadastrar">
            </a>
        </div>      
    </body>
</html>
