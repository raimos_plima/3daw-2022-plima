-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 10-Jun-2022 às 14:59
-- Versão do servidor: 8.0.27
-- versão do PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `ecoescambo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `interesse`
--

DROP TABLE IF EXISTS `interesse`;
CREATE TABLE  `interesse` (
  `id_Utilizador` int DEFAULT NULL,
  `id_produto` int DEFAULT NULL,
  KEY `fk_interessado_utilizador` (`id_Utilizador`),
  KEY `fk_interessado_produto` (`id_produto`)
);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

DROP TABLE IF EXISTS `produto`;
CREATE TABLE IF NOT EXISTS `produto` (
  `id_produto` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100)  NOT NULL,
  `descricao` varchar(2000)  NOT NULL,
  `imagem` varchar(30) NOT NULL,
  `id_Utilizador` int DEFAULT NULL,
  PRIMARY KEY (`id_produto`),
  KEY `fk_produto_utilizador` (`id_Utilizador`)
) ;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id_produto`, `titulo`, `descricao`, `imagem`, `id_Utilizador`) VALUES
(4, 'Celular', 'Smartphone Samsung Galaxy S21 128GB 5G Wi-Fi Tela 6.2\\\'\\\' Dual Chip 8GB RAM Câmera Tripla + Selfie 10MP - Cinza', 'img/celular.jpg', NULL),
(6, 'Bicicleta', 'Bicicleta Aro 26 Ultra Bikes Masculino 18v Freios V-Brake', 'img/bike.jpg', 2),
(7, 'Video Game', 'Videogame PlayStation 2 Slim Standard Black Console', 'img/videoGame.jpg', 1),
(8, 'Fone', 'Fone de Ouvido Bluetooth JBL Tune 660NC On Ear Preto - JBLT660NCBLK', 'img/fone.jpg', 1),
(9, 'Monitor', 'Monitor LG 22\\\'\\\' LED Full HD', 'img/monitor.jpg', 1),
(10, 'Televisão', 'Smart TV HD LED 32” Samsung 32T4300A - Wi-Fi HDR 2 HDMI 1 USB', 'img/tv.jpg', 2),
(11, 'Radio', 'Caixa Multimidia Portatil Tuner FM Preta', 'img/radio.jpg', 2),
(12, 'Ventilador', 'Ventilador de Mesa Arno Ultra Silence Force VD40 40cm, 6 Pás, 126W - Preto', 'img/ventilador.jpg', 2),
(13, 'teste 5', 'teste 5', 'img/teste5.png', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `utilizador`
--

DROP TABLE IF EXISTS `utilizador`;
CREATE TABLE IF NOT EXISTS `utilizador` (
  `id_Utilizador` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `senha` varchar(40) NOT NULL,
  PRIMARY KEY (`id_Utilizador`)
) ;

--
-- Extraindo dados da tabela `utilizador`
--

INSERT INTO `utilizador` (`id_Utilizador`, `nome`, `email`, `senha`) VALUES
(1, 'Jamile', 'jamile@teste.com.br', 'patasdeGalinha1'),
(2, 'Marcio', 'mbelo@teste.com.br', 'patasdeGalinha1');

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `interesse`
--
ALTER TABLE `interesse`
  ADD CONSTRAINT `fk_interessado_produto` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id_produto`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_interessado_utilizador` FOREIGN KEY (`id_Utilizador`) REFERENCES `utilizador` (`id_Utilizador`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `fk_produto_utilizador` FOREIGN KEY (`id_Utilizador`) REFERENCES `utilizador` (`id_Utilizador`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
