<DOCTYPE html>
    <html lang="pt-br">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Catalogo</title>
        <link rel="stylesheet" href="catalogo.css">


    </head>

    <body>

        <div class="container">

            <nav id="menu">

                <ul>

                    <li><a href="">Catálogo</a></li>
                    <li><a href="/Meus_Produtos.php">Meus produtos</a></li>
                    <li><a href="/login/login.php">Logout</a></li>

                </ul>

            </nav>


            <div id="busca">

                <h1>Produtos</h1>

                <!-- <div  class="separador"> </div> -->

                <form>
                    <Label> Pesquisa: </Label>
                    <input id="pesquisa" type="text" name="pesquisa" value="">
                    <input class="botaoprincipal" type="submit">

                    <input class="bradio" type="radio" name="pesquisa" value="Todos">
                    <label for="Todos">Todos</label>

                    <input class="bradio" type="radio" name="pesquisa" value="Interesse">
                    <label for="Interesse">Tenho Interesse</label>
                </form>

            </div>

            <div class="separador"> </div>

            <div id="catalogo_produtos">

                <div class="card clearfix">

                    <div class="produto">
                        <img src="/produto/p1.jpg">
                    </div>

                    <div class="caixa_texto">
                        <h2> Alexa </h2>
                        <h3> Marcelo </h3>
                        <h3> 3 meses de uso </h3>
                        <section><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex voluptates reiciendis harum debitis modi natus obcaecati odit porro eaque veniam facere tenetur, possimus maxime autem quae. Vel tempore reiciendis omnis. </p> </section>
                    </div>
                    <div class="div_interesse">
                        <button type="submit" class="botao_interesse" >Interesse</button>
                        <!-- <label for="bInteresse" >Interesse </Label> -->
                    </div>

                </div>
                <div  class="separador_produto"> </div>

                <div class="card clearfix">

                    <div class="produto">
                        <img src="/produto/p2.jpg">
                    </div>

                    <div class="caixa_texto">
                        <h2> Mesa de Madeira</h2>
                        <h3> Ricardo </h3>
                        <h3> 9 meses de uso </h3>
                        <section><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex voluptates reiciendis harum debitis modi natus obcaecati odit porro eaque veniam facere tenetur, possimus maxime autem quae. Vel tempore reiciendis omnis. </p> </section>
                    </div>
                    <div class="div_interesse">
                        <button type="submit" class="botao_interesse" >Interesse</button>
                        <!-- <label for="bInteresse" >Interesse </Label> -->
                    </div>

                </div>
                <div  class="separador_produto"> </div>

                <div class="card clearfix">

                    <div class="produto">
                        <img src="/produto/p3.jpg">
                    </div>

                    <div class="caixa_texto">
                        <h2> PS5 </h2>
                        <h3> Jonatham </h3>
                        <h3> 12 meses de uso </h3>
                        <section><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex voluptates reiciendis harum debitis modi natus obcaecati odit porro eaque veniam facere tenetur, possimus maxime autem quae. Vel tempore reiciendis omnis. </p> </section>
                    </div>
                    <div class="div_interesse">
                        <button type="submit" class="botao_interesse" >Interesse</button>
                        <!-- <label for="bInteresse" >Interesse </Label> -->
                    </div>

                </div>
                <div  class="separador_produto"> </div>

            </div>


        </div>



    </body>

    </html>