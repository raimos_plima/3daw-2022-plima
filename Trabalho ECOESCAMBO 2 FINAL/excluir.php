<?php
    require_once(__DIR__ . "/Classes/Produto.php");
    session_start();
    $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_NUMBER_INT);
    $resultado = Produto::excluir($id);
    
    if($resultado){
        $_SESSION["excluido"] = "Produto excluido com sucesso !";
    }else{
        $_SESSION["excluido"] = "Erro ao excluir !";
    }
    header("Location: Meus_Produtos.php");