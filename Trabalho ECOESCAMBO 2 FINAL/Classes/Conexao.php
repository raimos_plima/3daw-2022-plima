<?php
abstract class Conexao{

    public static function abrirConexao(){
    $pdo = new PDO('mysql:host=localhost:3306;dbname=ecoescambo;charset=utf8mb4', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
    }
}