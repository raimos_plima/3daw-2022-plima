<?php
    require_once(__DIR__ . "/Classes/Produto.php");
    session_start();
    //novo produto        
    if(isset($_POST['titulo'])){
        $titulo = filter_input(INPUT_POST, "titulo", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $descricao = filter_input(INPUT_POST, "descricao", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        //imagem
        $dir = "img/";
        $file = $dir.basename($_FILES["foto"]["name"]);
        move_uploaded_file($_FILES["foto"]["tmp_name"], $file);

        //Banco
        //retirar null de id_utilizador
        $produto = new Produto($titulo, $descricao, $file, null);
        $resul = $produto->add();             
    } 
    
    //paginação banco de dados
    $qtd = 3;
    $atual = (isset($_GET['pag']))? intval($_GET['pag']):1;   
    $qtdProdutos = Produto::qtdRegMeus();
    $qtdPag = ceil($qtdProdutos/$qtd);
    $linhaInicial = ($atual-1)*$qtd;
    $dados = Produto::selecProdMeus($linhaInicial, $qtd);

    require_once("paginacao.php");

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">    
        <title>Meus produtos</title>
        <link rel="stylesheet" href="style.css"> 
    </head>
    <body>
          
        <div class="container">
            <nav id="menu">

                    <ul>

                        <li><a href="catalago.php">Catálogo</a></li>
                        <li><a href="Meus_Produtos.php">Meus produtos</a></li>
                        <li><a href="/login/login.php">Logout</a></li>

                    </ul>

            </nav>
            <script>
                function todosProd(){

                }
            </script>

            <div id="busca">
                <h2>Meus Produtos</h2>    
                <form action="Meus_Produtos.php" method="POST">
                    <p>
                        Filtrar:
                        <input type="radio" id="filtro" name="sel_filtro" value="todos" onclick="todosProd()" checked>
                        <label for="filtro">Todos</label>
                        <input type="radio" id="inter" name="sel_filtro" value="int" onclick="interProd()">
                        <label for="inter">Só nos quais há interessados</label>
                    </p>
                </form>
            </div>
            
            <div class="separador"> </div>

            <div id="meus_produtos">
                <table>
                    <?php                      
                        if(!$dados){
                            echo "<b>Você não possui produtos cadastrados !</b>";
                        }else{
                            foreach($dados as $value){ ?>
                        
                                <tr>
                                    <td>
                                        <img src='<?=$value->imagem?>' id='imgProd'>
                                    </td>
                                    <td id='descr'>
                                        <b><?=$value->titulo?></b><br><?=$value->descricao?>
                                    </td>                                
                                    <td>
                                    <form method="post" action="excluir.php" id="excluir">
                                        <a href='#'><img src='Botoes/Editar.png' alt='botão Editar' class='botoesint'></a></div>
                                        <input type="hidden" name = "id" value = "<?= $value->id_produto?>">
                                        <input type="image" src="Botoes/Excluir.png" class='botoesint'>
                                    </form>
                                        <!--<button type="submit" form="excluir"><img src='Botoes/Excluir.png' alt='botao Excluir' class='botoesint'></button><br>-->
                                        <a href='#'><img src='Botoes/interesses.png' alt='botao interesses' id='botoesint'></a>
                                    </td>
                                </tr>
                                
                        <?php }
                        }              
                                            
                    ?>
                </table>
            </div>
            <br>
            <?php
                paginacao($qtdPag, $atual);                 
            ?>
            
            <hr>
            <a href="cadastroProduto.php">
                <img src="Botoes/Cadastrar.png" alt="Botão cadastrar novo" id="cadastrar">
            </a>
        </div>      
    </body>
</html>
